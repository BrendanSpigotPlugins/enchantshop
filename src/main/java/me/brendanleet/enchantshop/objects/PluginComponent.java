package me.brendanleet.enchantshop.objects;

import me.brendanleet.enchantshop.EnchantShop;
import org.bukkit.Bukkit;
import org.bukkit.event.EventHandler;
import org.bukkit.event.HandlerList;
import org.bukkit.event.Listener;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.scheduler.BukkitTask;

import java.util.Arrays;

public class PluginComponent implements Listener {

    private EnchantShop plugin;

    public PluginComponent(EnchantShop plugin) {
        this(plugin, true);
    }

    public PluginComponent(EnchantShop plugin, boolean register) {
        this.plugin = plugin;

        if (register) {
            // Automatically register EventHandlers
            boolean hasEventHandler = Arrays.stream(getClass().getDeclaredMethods()).anyMatch((m) -> m.getAnnotation(EventHandler.class) != null);
            if (hasEventHandler) {
                Bukkit.getPluginManager().registerEvents(this, plugin);
            }
        }
    }

    public EnchantShop getPlugin() {
        return plugin;
    }

    public void setPlugin(EnchantShop plugin) {
        this.plugin = plugin;
    }

    public BukkitTask run(PluginRunnable task) {
        return new BukkitRunnable() {
            public void run() {
                try {
                    task.run();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }.runTask(getPlugin());
    }

    public BukkitTask runTask(PluginRunnable task) {
        return new BukkitRunnable() {
            public void run() {
                try {
                    task.run();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }.runTask(getPlugin());
    }

    public BukkitTask runLater(PluginRunnable task, int delay) {
        return new BukkitRunnable() {
            public void run() {
                try {
                    task.run();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }.runTaskLater(getPlugin(), delay);
    }

    public BukkitTask runAsync(PluginRunnable task) {
        return new BukkitRunnable() {
            public void run() {
                try {
                    task.run();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }.runTaskAsynchronously(getPlugin());
    }

    public BukkitTask runAsyncLater(PluginRunnable task, int delay) {
        return new BukkitRunnable() {
            public void run() {
                try {
                    task.run();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }.runTaskLaterAsynchronously(getPlugin(), delay);
    }

    public BukkitTask runTimer(PluginRunnable task, int period) {
        return new BukkitRunnable() {
            public void run() {
                try {
                    task.run();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }.runTaskTimer(getPlugin(), 0, period);
    }

    public BukkitTask runTimer(PluginRunnable task, int delay, int period) {
        return new BukkitRunnable() {
            public void run() {
                try {
                    task.run();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }.runTaskTimer(getPlugin(), delay, period);
    }

    public BukkitTask runTimer(PluginRunnable task, int delay, int period, int steps) {
        return new BukkitRunnable() {
            int step = 0;
            public void run() {
                if(step >= steps) {
                    this.cancel();
                }

                try {
                    task.run();
                } catch (Exception e) {
                    e.printStackTrace();
                }

                step++;
            }
        }.runTaskTimer(getPlugin(), delay, period);
    }

    public BukkitTask runTimerAsync(PluginRunnable task, int delay, int period) {
        return new BukkitRunnable() {
            public void run() {
                try {
                    task.run();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }.runTaskTimerAsynchronously(getPlugin(), delay, period);
    }

    public BukkitTask runTimerAsync(PluginRunnable task, int delay, int period, int steps) {
        return new BukkitRunnable() {
            int step = 0;
            public void run() {
                if(step >= steps) {
                    this.cancel();
                }

                try {
                    task.run();
                } catch (Exception e) {
                    e.printStackTrace();
                }

                step++;
            }
        }.runTaskTimer(getPlugin(), delay, period);
    }

    public void unregister() {
        HandlerList.unregisterAll(this);
    }

}
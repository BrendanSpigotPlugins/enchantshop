package me.brendanleet.enchantshop.objects;

public interface PluginRunnable {

    void run() throws Exception;
}
package me.brendanleet.enchantshop;

import lombok.Getter;
import me.brendanleet.enchantshop.commands.EnchantShopCommand;
import me.brendanleet.enchantshop.config.type.DefaultConfig;
import me.brendanleet.enchantshop.config.type.EnchantmentsConfig;
import me.brendanleet.enchantshop.config.type.MessagesConfig;
import me.brendanleet.enchantshop.enchant.EnchantManager;
import me.brendanleet.enchantshop.gui.ChangelogGUI;
import me.brendanleet.enchantshop.gui.EnchantsGUI;
import me.brendanleet.enchantshop.gui.PaymentTypeSelectGUI;
import me.brendanleet.enchantshop.listeners.ShopCreationHandler;
import me.brendanleet.enchantshop.listeners.ShopDeleteHandler;
import me.brendanleet.enchantshop.listeners.TransactionHandler;
import me.brendanleet.enchantshop.listeners.UpdateNotifyHandler;
import me.brendanleet.enchantshop.patchnotes.PatchNoteManager;
import me.brendanleet.enchantshop.transaction.type.MoneyTransaction;
import me.brendanleet.enchantshop.transaction.type.XPLevelTransaction;
import net.milkbowl.vault.economy.Economy;
import org.bukkit.plugin.RegisteredServiceProvider;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.logging.Level;

public class EnchantShop extends JavaPlugin {

    // TODO: Test and then publish

    @Getter
    private static EnchantShop instance;

    // Managers //
    @Getter
    private EnchantManager enchantManager;
    @Getter
    private Updater updater;
    @Getter
    private PatchNoteManager patchNoteManager;

    // GUIs //
    @Getter
    private PaymentTypeSelectGUI paymentTypeSelectGUI;
    @Getter
    private EnchantsGUI enchantsGUI;
    @Getter
    private ChangelogGUI changelogGUI;

    // Transactions //
    @Getter
    private XPLevelTransaction xpLevelTransaction;
    @Getter
    private MoneyTransaction moneyTransaction;

    // Configs //
    @Getter
    private DefaultConfig defaultConfig;
    @Getter
    private MessagesConfig messagesConfig;
    @Getter
    private EnchantmentsConfig enchantsConfig;

    // Vault //
    @Getter
    private Economy economy = null;

    public void onEnable() {
        instance = this;
        this.updater = new Updater(this, 14273);
        MetricsLite metrics = new MetricsLite(this);

        registerManagers();
        registerGUIs();
        registerConfigs();
        registerTransactions();
        registerCommands();
        registerHandlers();

        getDefaultConfig().load();
        getEnchantsConfig().load();
        getMessagesConfig().load();

        if (getDefaultConfig().isUpdateCheck()) {
            if (getUpdater().getUpdateCheckResult() == Updater.UpdateCheckResult.UP_TO_DATE) {
                getLogger().info("The server is running on the latest version of EnchantShop.");
            } else if (getUpdater().getUpdateCheckResult() == Updater.UpdateCheckResult.OUT_DATED) {
                getLogger().info("The server is running on an old version of EnchantShop. Your version is " + getUpdater().getCurrentVersionString() + " and the latest " +
                        "version is " + getUpdater().getLatestVersionString() + ". Download it at " + getUpdater().getResourceURL());
            } else if (getUpdater().getUpdateCheckResult() == Updater.UpdateCheckResult.UNRELEASED) {
                getLogger().info("Your server is running a development build of EnchantShop. Report any issues to BrendanLeeT (Not released on Spigot)");
            }
        }

        if (!setupEconomy()) {
            getLogger().log(Level.WARNING, "Cannot hook up into an economy class or use-vault is disabled in the config " +
                    "therefore the money transaction type is unavailable.");
        } else {
            getLogger().info("Successfully hooked up into Vault!");
        }
    }

    public void onDisable() {
        instance = null;
    }

    private void registerGUIs() {
        this.paymentTypeSelectGUI = new PaymentTypeSelectGUI(this);
        this.enchantsGUI = new EnchantsGUI(this);
        this.changelogGUI = new ChangelogGUI(this);
    }

    private void registerCommands() {
        getCommand("enchantshop").setExecutor(new EnchantShopCommand());
    }

    private void registerManagers() {
        this.enchantManager = new EnchantManager(this);
        this.patchNoteManager = new PatchNoteManager(this);
    }

    private void registerHandlers() {
        new TransactionHandler(this);
        new ShopCreationHandler(this);
        new ShopDeleteHandler(this);
        new UpdateNotifyHandler(this);
    }

    private void registerTransactions() {
        this.xpLevelTransaction = new XPLevelTransaction(this);
        this.moneyTransaction = new MoneyTransaction(this);
    }

    private void registerConfigs() {
        this.defaultConfig = new DefaultConfig(this);
        this.messagesConfig = new MessagesConfig(this);
        this.enchantsConfig = new EnchantmentsConfig(this);
    }

    private boolean setupEconomy() {
        if (!getDefaultConfig().isUseVault()) {
            return false;
        }
        if (getServer().getPluginManager().getPlugin("Vault") == null) {
            return false;
        }
        RegisteredServiceProvider<Economy> rsp = getServer().getServicesManager().getRegistration(Economy.class);
        if (rsp == null) {
            return false;
        }
        economy = rsp.getProvider();
        return economy != null;
    }
}

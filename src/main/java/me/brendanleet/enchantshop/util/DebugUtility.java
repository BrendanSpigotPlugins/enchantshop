package me.brendanleet.enchantshop.util;

import net.md_5.bungee.api.chat.ComponentBuilder;
import net.md_5.bungee.api.chat.HoverEvent;
import net.md_5.bungee.api.chat.TextComponent;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@SuppressWarnings({"unchecked", "unused", "rawtypes"})
public class DebugUtility {
    private static boolean enabled = true;
    private static List<UUID> debuggers = new ArrayList<>();

    public static boolean isEnabled() {
        return enabled;
    }

    public static void toggle() {
        enabled = !enabled;
    }

    public static void toggle(boolean b) {
        enabled = b;
    }

    public static void toggle(Player player) {
        if (!debuggers.contains(player.getUniqueId()))
            debuggers.add(player.getUniqueId());
        else debuggers.remove(player.getUniqueId());
    }

    public static void addDebugger(UUID uuid) {
        if (!debuggers.contains(uuid))
            debuggers.add(uuid);
    }

    public static void removeDebugger(UUID uuid) {
        if (debuggers.contains(uuid))
            debuggers.remove(uuid);
    }

    public static List<UUID> getDebuggers() {
        return debuggers;
    }


    /**
     * SEND TO ALL DEBUGGERS
     **/
    public static void debug(String msg) {
        if (!enabled) {
            return;
        }
        msg = colorize(msg);
        for (Player player : Bukkit.getOnlinePlayers()) {
            if (debuggers.contains(player.getUniqueId())) {
                player.sendMessage(" ");
                player.sendMessage(colorize("&7[DEBUG] &f" + msg));
            }
        }
    }

    /**
     * SEND TO ALL DEBUGGERS
     **/
    public static void debug(TextComponent component, String hover) {
        TextComponent output = new TextComponent(net.md_5.bungee.api.ChatColor.GRAY + "[DEBUG] " + net.md_5.bungee.api.ChatColor.RESET);
        output.addExtra(component);
        output.setHoverEvent( new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder(hover).create()));

        Bukkit.getOnlinePlayers().forEach(player -> {
            if (debuggers.contains(player.getUniqueId())) {
                player.sendMessage(" ");
                player.spigot().sendMessage(output);
            }
        });
    }

    private static String colorize(String message) {
        return ChatColor.translateAlternateColorCodes('&', message);
    }
}
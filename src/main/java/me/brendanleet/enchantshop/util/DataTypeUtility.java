package me.brendanleet.enchantshop.util;

public class DataTypeUtility {

    public static boolean isInteger(String number) {
        try {
            int x = Integer.parseInt(number);
        } catch (NumberFormatException e) {
            return false;
        }
        return true;
    }

    public static boolean isDouble(String number) {
        try {
            double x = Double.parseDouble(number);
        } catch (NumberFormatException e) {
            return false;
        }
        return true;
    }

    public static boolean isBoolean(String value) {
        try {
            boolean x = Boolean.parseBoolean(value);
        } catch (NumberFormatException e) {
            return false;
        }
        return true;
    }
}
package me.brendanleet.enchantshop.util;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.block.Action;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;

public class PlayerUtility {

    public static boolean isRightClick(Action action){
        return action == Action.RIGHT_CLICK_AIR || action == Action.RIGHT_CLICK_BLOCK;
    }

    public static boolean isRightClickAir(Action action){
        return action == Action.RIGHT_CLICK_AIR;
    }

    public static boolean isRightClickBlock(Action action){
        return action == Action.RIGHT_CLICK_BLOCK;
    }


    public static boolean isLeftClick(Action action){
        return action == Action.LEFT_CLICK_AIR || action == Action.LEFT_CLICK_BLOCK;
    }

    public static boolean isLeftClickAir(Action action){
        return action == Action.LEFT_CLICK_AIR;
    }

    public static boolean isLeftClickBlock(Action action){
        return action == Action.LEFT_CLICK_BLOCK;
    }

    public static int getEmptySlots(Player player) {
        PlayerInventory inventory = player.getInventory();
        ItemStack[] cont = inventory.getContents();
        int i = 0;
        for (ItemStack item : cont)
            if (item != null && item.getType() != Material.AIR) {
                i++;
            }
        return 36 - i;
    }
}
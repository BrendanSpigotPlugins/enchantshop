package me.brendanleet.enchantshop.util;

import org.bukkit.ChatColor;

public class StringUtility {

    public static String newLine() {
        return System.lineSeparator();
    }

    public static String color(String message) {
        return ChatColor.translateAlternateColorCodes('&', message);
    }

    public static String getEnchantName(String enchant) {
        switch (enchant.toLowerCase()) {
            // 1.9+
            case "aqua_affinity":
            case "bane_of_arthropods":
            case "blast_protection":
            case "channeling":
            case "smite":
            case "silk_touch":
            case "sharpness":
            case "riptide":
            case "respiration":
            case "quick_charge":
            case "punch":
            case "protection":
            case "projectile_protection":
            case "power":
            case "piercing":
            case "multishot":
            case "mending":
            case "lure":
            case "luck_of_the_sea":
            case "loyalty":
            case "looting":
            case "knockback":
            case "infinity":
            case "impaling":
            case "frost_walker":
            case "fortune":
            case "flame":
            case "fire_protection":
            case "fire_aspect":
            case "feather_falling":
            case "efficiency":
            case "depth_strider":
            case "unbreaking":
            case "thorns":
                return simpleEnchant(enchant.toLowerCase());
            case "multshot":
                return "Multishot";
            case "binding_curse":
                return "Curse of Binding";
            case "vanishing_curse":
                return "Curse of Vanishing";
            case "sweeping":
                return "Sweeping Edge";

            // 1.7 - 1.8
            case "protection_environmental":
                return "Protection";
            case "dig_speed":
                return "Efficiency";
            case "protection_fire":
                return "Fire Protection";
            case "protection_fall":
                return "Feather Falling";
            case "durability":
                return "Unbreaking";
            case "protection_explosions":
                return "Blast Protection";
            case "loot_bonus_blocks":
                return "Fortune";
            case "protection_projectile":
                return "Projectile Protection";
            case "oxygen":
                return "Respiration";
            case "water_worker":
                return "Aqua Affinity";
            case "damage_all":
                return "Sharpness";
            case "arrow_damage":
                return "Power";
            case "damage_undead":
                return "Smite";
            case "arrow_knockback":
                return "Punch";
            case "damage_arthropods":
                return "Bane of Arthropods";
            case "arrow_fire":
                return "flame";
            case "arrow_infinite":
                return "Infinity";
            case "loot_bonus_mobs":
                return "Looting";
            case "luck":
                return "Luck of the sea";
            default:
                return toTitleCase(enchant);
        }
    }

    protected static String simpleEnchant(String givenString) {
        String[] arr = givenString.split(" ");
        StringBuffer sb = new StringBuffer();

        for (int i = 0; i < arr.length; i++) {
            sb.append(Character.toUpperCase(arr[i].charAt(0)))
                    .append(arr[i].substring(1)).append(" ");
        }
        return sb.toString().trim().replace("_", " ");
    }

    public static String toTitleCase(String givenString) {
        String[] arr = givenString.split(" ");
        StringBuffer sb = new StringBuffer();

        for (int i = 0; i < arr.length; i++) {
            sb.append(Character.toUpperCase(arr[i].charAt(0)))
                    .append(arr[i].substring(1)).append(" ");
        }
        return sb.toString().trim();
    }
}

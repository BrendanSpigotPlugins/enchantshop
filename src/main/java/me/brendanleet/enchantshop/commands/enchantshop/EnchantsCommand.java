package me.brendanleet.enchantshop.commands.enchantshop;

import me.brendanleet.enchantshop.EnchantShop;
import me.brendanleet.enchantshop.commands.EnchantShopCommand;
import me.brendanleet.enchantshop.commands.other.CommandMethod;
import me.brendanleet.enchantshop.objects.PluginComponent;
import me.brendanleet.enchantshop.util.DebugUtility;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class EnchantsCommand extends PluginComponent implements CommandMethod {

    public EnchantsCommand(EnchantShop plugin) {
        super(plugin);
    }

    public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args) {
        if (sender instanceof Player) {
            Player player = (Player) sender;
            if (player.hasPermission(EnchantShopCommand.commandPermissions.get(args[0])))
                getPlugin().getEnchantsGUI().init(player);
        }
        return false;
    }
}
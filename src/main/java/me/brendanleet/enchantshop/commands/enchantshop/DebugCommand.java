package me.brendanleet.enchantshop.commands.enchantshop;

import me.brendanleet.enchantshop.EnchantShop;
import me.brendanleet.enchantshop.commands.EnchantShopCommand;
import me.brendanleet.enchantshop.commands.other.CommandMethod;
import me.brendanleet.enchantshop.objects.PluginComponent;
import me.brendanleet.enchantshop.util.DebugUtility;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class DebugCommand extends PluginComponent implements CommandMethod {

    public DebugCommand(EnchantShop plugin) {
        super(plugin);
    }

    public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args) {
        if (sender instanceof Player) {
            Player player = (Player) sender;
            if (player.hasPermission(EnchantShopCommand.commandPermissions.get(args[0]))) {
                DebugUtility.toggle(player);
                player.sendMessage(ChatColor.GREEN + "Debugging mode set to " + DebugUtility.getDebuggers().contains(player.getUniqueId()));
                player.sendMessage(ChatColor.GRAY.toString() + ChatColor.ITALIC + "For extra details, hover your mouse over the debug message");
            }
        }
        return false;
    }
}
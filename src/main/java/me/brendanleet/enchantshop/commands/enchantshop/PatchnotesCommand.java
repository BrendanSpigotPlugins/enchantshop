package me.brendanleet.enchantshop.commands.enchantshop;

import me.brendanleet.enchantshop.EnchantShop;
import me.brendanleet.enchantshop.Updater;
import me.brendanleet.enchantshop.commands.EnchantShopCommand;
import me.brendanleet.enchantshop.commands.other.CommandMethod;
import me.brendanleet.enchantshop.objects.PluginComponent;
import me.brendanleet.enchantshop.patchnotes.PatchNoteType;
import me.brendanleet.enchantshop.util.StringUtility;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class PatchnotesCommand extends PluginComponent implements CommandMethod {

    public PatchnotesCommand(EnchantShop plugin) {
        super(plugin);
    }

    public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args) {
        runAsync(() -> {
            if (sender instanceof Player) {
                Player player = (Player) sender;
                if (player.hasPermission(EnchantShopCommand.commandPermissions.get(args[0]))) {
                    if (args.length != 2) {
                        player.sendMessage(ChatColor.RED + "/enchantshop patchnotes <ALL|CHANGES|NEW_ADDITIONS|BUG_FIXES>");
                        return;
                    }

                    switch (args[1].toLowerCase()) {
                        case "bug_fixes":
                            getPlugin().getChangelogGUI().init(player, PatchNoteType.BUG_FIXES);
                            break;
                        case "changes":
                            getPlugin().getChangelogGUI().init(player, PatchNoteType.CHANGE);
                            break;
                        case "new_additions":
                            getPlugin().getChangelogGUI().init(player, PatchNoteType.NEW_ADDITIONS);
                            break;
                        case "all":
                            getPlugin().getChangelogGUI().init(player, PatchNoteType.ALL);
                            break;
                        default:
                            player.sendMessage(ChatColor.RED + "Valid patchnote types: all, changes, new_additions, or bug_fixes");
                    }
                }
            }
        });
        return false;
    }
}
package me.brendanleet.enchantshop.commands.enchantshop;

import me.brendanleet.enchantshop.EnchantShop;
import me.brendanleet.enchantshop.Updater;
import me.brendanleet.enchantshop.commands.EnchantShopCommand;
import me.brendanleet.enchantshop.commands.other.CommandMethod;
import me.brendanleet.enchantshop.objects.PluginComponent;
import me.brendanleet.enchantshop.util.StringUtility;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class CheckUpdateCommand extends PluginComponent implements CommandMethod {

    public CheckUpdateCommand(EnchantShop plugin) {
        super(plugin);
    }

    public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args) {
        runAsync(() -> {
            if (sender instanceof Player) {
                Player player = (Player) sender;
                if (player.hasPermission(EnchantShopCommand.commandPermissions.get(args[0]))) {
                    String currentVersion = getPlugin().getUpdater().getCurrentVersionString();
                    String newVersion = getPlugin().getUpdater().getLatestVersionString();
                    String updateURL = getPlugin().getUpdater().getResourceURL();

                    if (getPlugin().getUpdater().getUpdateCheckResult() == Updater.UpdateCheckResult.OUT_DATED) {
                        player.sendMessage(StringUtility.color("&6[EnchantShop] &eThe server is running on an old version of EnchantShop. Your version is &a" + currentVersion + " &eand the latest " +
                                "version is &a" + newVersion + ". &eDownload it at &a" + updateURL));
                    } else if (getPlugin().getUpdater().getUpdateCheckResult() == Updater.UpdateCheckResult.UNRELEASED) {
                        player.sendMessage(StringUtility.color("&6[EnchantShop] &eYour server is running a &adevelopment build &eof EnchantShop. Report any issues to BrendanLeeT (Not released on Spigot)"));
                    } else if (getPlugin().getUpdater().getUpdateCheckResult() == Updater.UpdateCheckResult.UP_TO_DATE) {
                        player.sendMessage(StringUtility.color("&6[EnchantShop] &eThe server is running on the most up to date version of the plugin, no downloads are required."));
                    }
                }
            }
        });
        return false;
    }
}
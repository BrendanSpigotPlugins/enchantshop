package me.brendanleet.enchantshop.commands.other;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;

public interface CommandMethod {

    public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args);

}
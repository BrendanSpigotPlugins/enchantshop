package me.brendanleet.enchantshop.commands;

import me.brendanleet.enchantshop.EnchantShop;
import me.brendanleet.enchantshop.commands.enchantshop.CheckUpdateCommand;
import me.brendanleet.enchantshop.commands.enchantshop.DebugCommand;
import me.brendanleet.enchantshop.commands.enchantshop.EnchantsCommand;
import me.brendanleet.enchantshop.commands.enchantshop.PatchnotesCommand;
import me.brendanleet.enchantshop.commands.other.CommandMethod;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabCompleter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class EnchantShopCommand implements CommandExecutor, TabCompleter {

    public EnchantShop plugin;

    private static HashMap<String, CommandMethod> commands = new HashMap<>();
    public static HashMap<String, String> commandPermissions = new HashMap<>();
    private static HashMap<String, String> descriptions = new HashMap<>();

    public EnchantShopCommand() {
        register("debug", new DebugCommand(EnchantShop.getInstance()), "Toggles the debugger mode", "enchantshop.command.debug");
        register("checkupdate", new CheckUpdateCommand(EnchantShop.getInstance()), "Checks to see if the plugin requires an update", "enchantshop.command.checkupdate");
        register("enchants", new EnchantsCommand(EnchantShop.getInstance()), "Visually displays all of the Enchantments  + Aliases in a GUI", "enchantshop.command.enchants");
        register("patchnotes", new PatchnotesCommand(EnchantShop.getInstance()), "Displays bug fixes + changes in a GUI", "enchantshop.command.enchants");
    }

    private void register(String name, CommandMethod cmd, String description, String permissionNode) {
        commands.put(name.toLowerCase(), cmd);
        descriptions.put(name.toLowerCase(), description);
        if (permissionNode != null)
            commandPermissions.put(name.toLowerCase(), permissionNode);
    }



    private boolean exists(String name) {
        return commands.containsKey(name);
    }

    private CommandMethod getExecutor(String name) {
        return commands.get(name);
    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args) {
        if (hasPermission(sender)) {
            if (args.length == 0) {
                sendHelp(sender, cmd.getName());
                return true;
            }
            if (exists(args[0].toLowerCase())) {
                getExecutor(args[0].toLowerCase()).onCommand(sender, cmd, commandLabel, args);
                return true;
            } else {
                sendHelp(sender, cmd.getName());
                return true;
            }
        } else {
            sender.sendMessage(ChatColor.RED + "You do not have permission to execute this command");
        }

        return false;
    }

    private void sendHelp(CommandSender sender, String cmd) {
        sender.sendMessage(ChatColor.DARK_GRAY.toString() + ChatColor.STRIKETHROUGH + "------------------------------------------");
        sender.sendMessage(ChatColor.GOLD.toString() + ChatColor.BOLD + "EnchantShop Commands");
        sender.sendMessage(" ");
        for (String commands : commands.keySet()) {
            if (commandPermissions.containsKey(commands) && !sender.hasPermission(commandPermissions.get(commands)))
                continue;
            sender.sendMessage(ChatColor.YELLOW + "/" + cmd + " " + commands + ChatColor.DARK_GRAY + " - " + ChatColor.GRAY + descriptions.get(commands));
        }
        sender.sendMessage(ChatColor.DARK_GRAY.toString() + ChatColor.STRIKETHROUGH + "------------------------------------------");
    }

    private boolean hasPermission(CommandSender sender) {
        for (String command : commands.keySet())
            if (commandPermissions.containsKey(command) && !sender.hasPermission(commandPermissions.get(command)))
                return false;

        return true;
    }

    private boolean hasPermission(CommandSender sender, String command) {
        return !commandPermissions.containsKey(command) || commandPermissions.containsKey(command) && sender.hasPermission(commandPermissions.get(command));

    }

    @Override
    public List<String> onTabComplete(CommandSender sender, Command command, String commandLabel, String[] args) {
        List<String> output = new ArrayList<>();
        if (args.length == 1) {
            for (String commands : commands.keySet()) {
                if (hasPermission(sender, commands)) {
                    output.add(commands);
                }
            }
        }

        return output.isEmpty() ? null : output;
    }
}
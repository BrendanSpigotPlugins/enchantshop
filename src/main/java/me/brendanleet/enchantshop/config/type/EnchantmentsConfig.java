package me.brendanleet.enchantshop.config.type;

import lombok.Getter;
import me.brendanleet.enchantshop.EnchantShop;
import me.brendanleet.enchantshop.config.ConfigInterface;
import me.brendanleet.enchantshop.objects.PluginComponent;
import me.brendanleet.enchantshop.util.Config;
import me.brendanleet.enchantshop.util.StringUtility;
import org.bukkit.ChatColor;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.enchantments.Enchantment;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;

public class EnchantmentsConfig extends PluginComponent implements ConfigInterface {

    private Config config = new Config("enchantments", EnchantShop.getInstance());

    @Getter
    private boolean usingEnchantPermissions;

    public EnchantmentsConfig(EnchantShop plugin) {
        super(plugin);
    }

    @Override
    public void loadDefault() {
        getConfig().addDefault("use-permissions", false);

        for (Enchantment enchant : Enchantment.values()) {
            ArrayList<String> alias = new ArrayList<>();
            alias.add(StringUtility.getEnchantName(enchant.getName()));
            getConfig().addDefault(enchant.getName() + ".aliases", alias);
            getConfig().addDefault(enchant.getName() + ".permission", "enchantshop.enchant." + enchant.getName().toLowerCase());
        }

        getConfig().options().copyDefaults(true);
        config.save();
    }

    @Override
    public void load() {
        loadDefault();
        usingEnchantPermissions = getConfig().getBoolean("use-permissions");

        getConfig().getKeys(false).forEach(enchantment -> {
            List<String> aliases = getConfig().getStringList(enchantment + ".aliases");
            getPlugin().getEnchantManager().getEnchantments().put(Enchantment.getByName(enchantment), aliases);
        });

        getConfig().getKeys(false).forEach(enchantment -> {
            String permission = getConfig().getString(enchantment + ".permission");
            getPlugin().getEnchantManager().getEnchantmentPermissions().put(Enchantment.getByName(enchantment), permission);
        });

        getPlugin().getLogger().log(Level.INFO, "Registered " + getPlugin().getEnchantManager().getEnchantments().size() + " enchants and aliases");
    }

    @Override
    public void save() {
    }

    @Override
    public FileConfiguration getConfig() {
        return config.getConfig();
    }
}

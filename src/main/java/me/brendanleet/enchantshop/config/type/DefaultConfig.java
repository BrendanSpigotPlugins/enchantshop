package me.brendanleet.enchantshop.config.type;

import lombok.Getter;
import me.brendanleet.enchantshop.EnchantShop;
import me.brendanleet.enchantshop.config.ConfigInterface;
import me.brendanleet.enchantshop.objects.PluginComponent;
import me.brendanleet.enchantshop.util.StringUtility;
import org.bukkit.configuration.file.FileConfiguration;

public class DefaultConfig extends PluginComponent implements ConfigInterface {

    //private Config config = new Config("features", EnchantShop.getInstance());

    @Getter
    private boolean updateCheck;
    @Getter
    private String signTitle;
    @Getter
    private boolean useVault;


    public DefaultConfig(EnchantShop plugin) {
        super(plugin);
    }

    @Override
    public void loadDefault() {
        getConfig().addDefault("check-for-updates", true);
        getConfig().addDefault("sign-title", "&8[&3Enchant&8]");
        getConfig().addDefault("use-vault", true);
        getConfig().options().copyDefaults(true);
        getPlugin().saveConfig();
    }

    @Override
    public void load() {
        loadDefault();
        updateCheck = getConfig().getBoolean("check-for-updates");
        signTitle = StringUtility.color(getConfig().getString("sign-title"));
        useVault = getConfig().getBoolean("use-vault");
    }

    @Override
    public void save() {
        getPlugin().saveConfig();
    }

    @Override
    public FileConfiguration getConfig() {
        return getPlugin().getConfig();
    }
}

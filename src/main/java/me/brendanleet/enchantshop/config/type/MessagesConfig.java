package me.brendanleet.enchantshop.config.type;

import lombok.Getter;
import me.brendanleet.enchantshop.EnchantShop;
import me.brendanleet.enchantshop.config.ConfigInterface;
import me.brendanleet.enchantshop.objects.PluginComponent;
import me.brendanleet.enchantshop.util.Config;
import me.brendanleet.enchantshop.util.StringUtility;
import org.bukkit.ChatColor;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.enchantments.Enchantment;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;

public class MessagesConfig extends PluginComponent implements ConfigInterface {

    private Config config = new Config("messages", EnchantShop.getInstance());

    @Getter
    private static Map<String, String> messages = new HashMap<>();

    public MessagesConfig(EnchantShop plugin) {
        super(plugin);
    }

    @Override
    public void loadDefault() {
        getConfig().addDefault("purchase-success", "&eYou have successfully purchased &a{enchantment} &efor &a{amount} &a{type}");
        getConfig().addDefault("not-found-funds", "&cYou do not have enough {type} to purchase {enchantment}");
        getConfig().addDefault("shop-created", "&eYou have successfully created a shop!");
        getConfig().addDefault("payment-type-set", "&eYou have set the payment type to &a{type}");
        getConfig().addDefault("shop-removed", "&eYou have removed the shop sign!");
        getConfig().addDefault("shop-removed-noperms", "&cYou do not have permissions to remove the shop sign");
        getConfig().addDefault("enchant-not-found", "&cCould not find the enchantment");
        getConfig().addDefault("no-item-in-hand", "&cYou do not have an item in your hand!");
        getConfig().addDefault("cannot-add-enchantment", "&cYou cannot add {enchantment} to the item in your hand");
        getConfig().addDefault("attempted-downgrade", "&cYou already have a better enchantment than the one you are about to purchase");
        getConfig().addDefault("no-permission-for-enchant", "&cYou do not have permission to purchase the {enchantment}");

        getConfig().options().copyDefaults(true);
        config.save();
    }

    @Override
    public void load() {
        loadDefault();
        getConfig().getKeys(false).forEach(message -> getMessages().put(message, ChatColor.translateAlternateColorCodes('&', getConfig().getString(message))));
        getPlugin().getLogger().log(Level.INFO, "Registered " + getMessages().size() + " messages");
    }

    @Override
    public void save() {
    }

    @Override
    public FileConfiguration getConfig() {
        return config.getConfig();
    }
}

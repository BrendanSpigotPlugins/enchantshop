package me.brendanleet.enchantshop.config;

import org.bukkit.configuration.file.FileConfiguration;

public interface ConfigInterface {

    void loadDefault();
    void load();
    void save();
    FileConfiguration getConfig();
}

package me.brendanleet.enchantshop.listeners;

import me.brendanleet.enchantshop.EnchantShop;
import me.brendanleet.enchantshop.config.type.MessagesConfig;
import me.brendanleet.enchantshop.objects.PluginComponent;
import me.brendanleet.enchantshop.util.DebugUtility;
import me.brendanleet.enchantshop.util.StringUtility;
import net.md_5.bungee.api.chat.TextComponent;
import org.bukkit.ChatColor;
import org.bukkit.block.Sign;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.SignChangeEvent;

public class ShopDeleteHandler extends PluginComponent {

    public ShopDeleteHandler(EnchantShop plugin) {
        super(plugin, true);
    }

    @EventHandler
    public void onSignEdit(BlockBreakEvent event) {
        Player player = event.getPlayer();
        if (event.getBlock() != null && event.getBlock().getState() instanceof Sign) {
            Sign sign = (Sign) event.getBlock().getState();
            if (player.hasPermission("enchantshop.deletesign") && getPlugin().getEnchantManager().isEnchantSign(sign)) {
                player.sendMessage(MessagesConfig.getMessages().get("shop-removed"));

                DebugUtility.debug(new TextComponent(player.getName() + " has removed a shop"),
                        "Location: {x}, {y}, {z}|Enchant: {enchant}|Level: {level}|Cost: {cost}"
                                .replace("{x}", String.valueOf(player.getLocation().getBlockX()))
                                .replace("{y}", String.valueOf(player.getLocation().getBlockY()))
                                .replace("{z}", String.valueOf(player.getLocation().getBlockZ()))
                                .replace("|", StringUtility.newLine()));
            } else if (!player.hasPermission("enchantshop.deletesign") && getPlugin().getEnchantManager().isEnchantSign(sign)) {
                event.setCancelled(true);
                player.sendMessage(MessagesConfig.getMessages().get("shop-removed-noperms"));
            }
        }
    }
}

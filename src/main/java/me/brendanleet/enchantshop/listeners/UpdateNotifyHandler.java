package me.brendanleet.enchantshop.listeners;

import me.brendanleet.enchantshop.EnchantShop;
import me.brendanleet.enchantshop.Updater;
import me.brendanleet.enchantshop.objects.PluginComponent;
import me.brendanleet.enchantshop.util.StringUtility;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.player.PlayerJoinEvent;

public class UpdateNotifyHandler extends PluginComponent {

    public UpdateNotifyHandler(EnchantShop plugin) {
        super(plugin, true);
    }

    @EventHandler
    public void onPlayerJoin(PlayerJoinEvent event) {
        runAsyncLater(() -> {
            Player player = event.getPlayer();
            if (player.hasPermission("enchantshop.notify-update") && getPlugin().getDefaultConfig().isUpdateCheck()) {
                String currentVersion = getPlugin().getUpdater().getCurrentVersionString();
                String newVersion = getPlugin().getUpdater().getLatestVersionString();
                String updateURL = getPlugin().getUpdater().getResourceURL();

                if (getPlugin().getUpdater().getUpdateCheckResult() == Updater.UpdateCheckResult.OUT_DATED) {
                    player.sendMessage(StringUtility.color("&6[EnchantShop] &eThe server is running on an old version of EnchantShop. Your version is &a" + currentVersion + " &eand the latest " +
                            "version is &a" + newVersion + ". &eDownload it at &a" + updateURL));
                } else if (getPlugin().getUpdater().getUpdateCheckResult() == Updater.UpdateCheckResult.UNRELEASED) {
                    player.sendMessage(StringUtility.color("&6[EnchantShop] &eYour server is running a &adevelopment build &eof EnchantShop. Report any issues to BrendanLeeT (Not released on Spigot)"));
                }
            }
        }, 20 * 2);
    }
}

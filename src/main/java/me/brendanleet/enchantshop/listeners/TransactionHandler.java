package me.brendanleet.enchantshop.listeners;

import me.brendanleet.enchantshop.EnchantShop;
import me.brendanleet.enchantshop.config.type.MessagesConfig;
import me.brendanleet.enchantshop.objects.PluginComponent;
import me.brendanleet.enchantshop.util.DebugUtility;
import me.brendanleet.enchantshop.util.PlayerUtility;
import me.brendanleet.enchantshop.util.StringUtility;
import net.md_5.bungee.api.chat.TextComponent;
import org.bukkit.Material;
import org.bukkit.block.Sign;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.player.PlayerInteractEvent;

public class TransactionHandler extends PluginComponent {

    public TransactionHandler(EnchantShop plugin) {
        super(plugin, true);
    }

    @EventHandler
    public void onSignInteract(PlayerInteractEvent event) {
        Player player = event.getPlayer();
        if (event.getClickedBlock() != null && PlayerUtility.isRightClickBlock(event.getAction()) && event.getClickedBlock().getState() instanceof Sign) {
            Sign sign = (Sign) event.getClickedBlock().getState();
            if (getPlugin().getEnchantManager().isEnchantSign(sign)) {
                Enchantment enchantment = getPlugin().getEnchantManager().getEnchant(sign);
                if (enchantment == null) {
                    player.sendMessage(MessagesConfig.getMessages().get("enchant-not-found"));
                    DebugUtility.debug("Couldn't find the enchantment " + sign.getLine(1));
                    return;
                }

                if (!getPlugin().getEnchantManager().hasPermission(player, enchantment)) {
                    player.sendMessage(MessagesConfig.getMessages().get("no-permission-for-enchant")
                            .replace("{enchantment}", sign.getLine(1)));
                    return;
                }

                if (player.getItemInHand() == null || player.getItemInHand().getType() == Material.AIR) {
                    player.sendMessage(MessagesConfig.getMessages().get("no-item-in-hand"));
                    DebugUtility.debug(new TextComponent(player.getName() + " tried to apply an enchantment but they have no item in their hand"),
                            "Item in hand: " + (player.getItemInHand() == null ? "nothing" : player.getItemInHand().getType()));
                    return;
                }

                if (!enchantment.canEnchantItem(player.getItemInHand())) {
                    player.sendMessage(MessagesConfig.getMessages().get("cannot-add-enchantment")
                            .replace("{enchantment}", StringUtility.getEnchantName(enchantment.getName())));
                    DebugUtility.debug(player.getName() + " tried to apply enchantment " + enchantment.getName() + " to " + player.getItemInHand().getType() + " but failed");
                    return;
                }

                if (player.getItemInHand().containsEnchantment(enchantment) && player.getItemInHand().getEnchantmentLevel(enchantment) >= getPlugin().getEnchantManager().getEnchantLevel(sign)) {
                    player.sendMessage(MessagesConfig.getMessages().get("attempted-downgrade"));
                    DebugUtility.debug(new TextComponent(player.getName() + " tried to downgrade their item"),
                            "Item: {item}|Enchant: {enchant}|{calc}"
                                    .replace("{item}", player.getItemInHand().getType().name())
                                    .replace("{calc}", player.getItemInHand().getEnchantmentLevel(enchantment) + ">=" + getPlugin().getEnchantManager().getEnchantLevel(sign))
                                    .replace("{enchant}", enchantment.getName())
                                    .replace("|", StringUtility.newLine()));
                    return;
                }

                if (getPlugin().getEnchantManager().shouldPayWithCash(sign)) {
                    getPlugin().getMoneyTransaction().doTransaction(player, sign);
                    DebugUtility.debug(player.getName() + " has called the transaction method for money");
                } else {
                    getPlugin().getXpLevelTransaction().doTransaction(player, sign);
                    DebugUtility.debug(player.getName() + " has called the transaction method for XP");
                }
            }
        }
    }
}
package me.brendanleet.enchantshop.listeners;

import me.brendanleet.enchantshop.EnchantShop;
import me.brendanleet.enchantshop.config.type.MessagesConfig;
import me.brendanleet.enchantshop.objects.PluginComponent;
import me.brendanleet.enchantshop.util.DebugUtility;
import me.brendanleet.enchantshop.util.StringUtility;
import net.md_5.bungee.api.chat.TextComponent;
import org.bukkit.ChatColor;
import org.bukkit.block.Sign;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.block.SignChangeEvent;

public class ShopCreationHandler extends PluginComponent {

    public ShopCreationHandler(EnchantShop plugin) {
        super(plugin, true);
    }

    @EventHandler
    public void onSignEdit(SignChangeEvent event) {
        Player player = event.getPlayer();
        if (player.hasPermission("enchantshop.createsign") && event.getLine(0) != null && event.getLine(0).contains(ChatColor.stripColor(getPlugin().getDefaultConfig().getSignTitle()))) {
            if (getPlugin().getEnchantManager().getEnchant(event.getLine(1)) == null) {
                player.sendMessage(ChatColor.RED + "You need to put a valid enchantment on the sign.");
                return;
            }

            if (getPlugin().getEnchantManager().getPriceByString(event.getLine(3)) <= 0) {
                player.sendMessage(ChatColor.RED + "You need to put a value greater than 0 on the sign");
                return;
            }

            event.setLine(0, getPlugin().getDefaultConfig().getSignTitle());
            player.sendMessage(MessagesConfig.getMessages().get("shop-created"));

            if (getPlugin().getEconomy() != null) {
                getPlugin().getPaymentTypeSelectGUI().init(player, (Sign) event.getBlock().getState(), event.getLines());
            } else {
                event.setLine(3, event.getLine(3) + " Levels");
            }

            DebugUtility.debug(new TextComponent(player.getName() + " has created a shop"),
                    "Location: {x}, {y}, {z}|Enchant: {enchant}|Level: {level}|Cost: {cost}|Economy Found: {vaultFound}"
                            .replace("{x}", String.valueOf(player.getLocation().getBlockX()))
                            .replace("{y}", String.valueOf(player.getLocation().getBlockY()))
                            .replace("{z}", String.valueOf(player.getLocation().getBlockZ()))
                            .replace("{enchant}", event.getLine(1))
                            .replace("{level}", event.getLine(2))
                            .replace("{cost}", event.getLine(3))
                            .replace("{vaultFound}", String.valueOf(getPlugin().getEconomy() != null))
                            .replace("|", StringUtility.newLine()));
        }
    }
}
package me.brendanleet.enchantshop.transaction;

import org.bukkit.block.Sign;
import org.bukkit.entity.Player;

public interface Transaction {

    TransactionTypeEnum getTransactionType();
    void doTransaction(Player player, Sign sign);
    int getCost(Player player, Sign sign);
    boolean hasEnough(Player player, Sign sign);
}

package me.brendanleet.enchantshop.transaction.type;

import me.brendanleet.enchantshop.EnchantShop;
import me.brendanleet.enchantshop.config.type.MessagesConfig;
import me.brendanleet.enchantshop.objects.PluginComponent;
import me.brendanleet.enchantshop.transaction.Transaction;
import me.brendanleet.enchantshop.transaction.TransactionTypeEnum;
import me.brendanleet.enchantshop.util.DebugUtility;
import me.brendanleet.enchantshop.util.StringUtility;
import net.md_5.bungee.api.chat.TextComponent;
import org.bukkit.block.Sign;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;

public class XPLevelTransaction extends PluginComponent implements Transaction {

    public XPLevelTransaction(EnchantShop plugin) {
        super(plugin);
    }

    @Override
    public TransactionTypeEnum getTransactionType() {
        return TransactionTypeEnum.XP;
    }

    @Override
    public void doTransaction(Player player, Sign sign) {
        Enchantment enchant = getPlugin().getEnchantManager().getEnchant(sign);
        if (enchant != null && hasEnough(player, sign)) {
            player.setLevel(player.getLevel() - getCost(player, sign));
            player.getItemInHand().addUnsafeEnchantment(enchant, getPlugin().getEnchantManager().getEnchantLevel(sign));
            player.sendMessage(MessagesConfig.getMessages().get("purchase-success")
                    .replace("{amount}", String.valueOf(getCost(player, sign)))
                    .replace("{type}", "xp")
                    .replace("{enchantment}", StringUtility.getEnchantName(enchant.getName())));

            DebugUtility.debug(new TextComponent(player.getName() + " made a purchase"),
                    "Enchant: {enchant}|Cost: {cost}|Item: {item}"
                            .replace("{item}", player.getItemInHand().getType().name())
                            .replace("{cost}", String.valueOf(getCost(player, sign)))
                            .replace("{enchant}", enchant.getName())
                            .replace("|", StringUtility.newLine()));
        } if (enchant != null && !hasEnough(player, sign)) {
            player.sendMessage(MessagesConfig.getMessages().get("not-found-funds")
                    .replace("{type}", "XP")
                    .replace("{amount}", String.valueOf(getCost(player, sign)))
                    .replace("{enchantment}", StringUtility.getEnchantName(enchant.getName())));
        }
    }

    @Override
    public int getCost(Player player, Sign sign) {
        return (getPlugin().getEnchantManager().getPriceBySign(sign));
    }

    @Override
    public boolean hasEnough(Player player, Sign sign) {
        int price = getPlugin().getEnchantManager().getPriceBySign(sign);
        return  price != -1 && player.getLevel() >= price;
    }
}

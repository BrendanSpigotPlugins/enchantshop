package me.brendanleet.enchantshop.transaction.type;

import me.brendanleet.enchantshop.EnchantShop;
import me.brendanleet.enchantshop.config.type.MessagesConfig;
import me.brendanleet.enchantshop.objects.PluginComponent;
import me.brendanleet.enchantshop.transaction.Transaction;
import me.brendanleet.enchantshop.transaction.TransactionTypeEnum;
import me.brendanleet.enchantshop.util.DebugUtility;
import me.brendanleet.enchantshop.util.StringUtility;
import net.md_5.bungee.api.chat.TextComponent;
import net.milkbowl.vault.economy.EconomyResponse;
import org.bukkit.Bukkit;
import org.bukkit.block.Sign;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;

public class MoneyTransaction extends PluginComponent implements Transaction {

    public MoneyTransaction(EnchantShop plugin) {
        super(plugin);
    }

    @Override
    public TransactionTypeEnum getTransactionType() {
        return TransactionTypeEnum.MONEY;
    }

    @Override
    public void doTransaction(Player player, Sign sign) {
        Enchantment enchant = getPlugin().getEnchantManager().getEnchant(sign);
        if (enchant != null && hasEnough(player, sign)) {
            EconomyResponse economyResponse = getPlugin().getEconomy().withdrawPlayer(player, getCost(player, sign));
            if (economyResponse.transactionSuccess()) {
                player.sendMessage(MessagesConfig.getMessages().get("purchase-success")
                        .replace("{amount}", String.valueOf(getCost(player, sign)))
                        .replace("{type}", "money")
                        .replace("{enchantment}", StringUtility.getEnchantName(enchant.getName())));

                DebugUtility.debug(new TextComponent(player.getName() + " made a purchase"),
                        "Enchant: {enchant}|Cost: {cost}|Item: {item}"
                                .replace("{item}", player.getItemInHand().getType().name())
                                .replace("{cost}", String.valueOf(getCost(player, sign)))
                                .replace("{enchant}", enchant.getName())
                                .replace("|", StringUtility.newLine()));

                player.getItemInHand().addUnsafeEnchantment(enchant, getPlugin().getEnchantManager().getEnchantLevel(sign));
            } else {
                player.sendMessage("There was an error processing this transaction.");
            }
        } else if (enchant != null && !hasEnough(player, sign)) {
            player.sendMessage(MessagesConfig.getMessages().get("not-found-funds")
                    .replace("{type}", "money")
                    .replace("{amount}", String.valueOf(getCost(player, sign)))
                    .replace("{enchantment}", StringUtility.getEnchantName(enchant.getName())));
        }
    }

    @Override
    public int getCost(Player player, Sign sign) {
        return (getPlugin().getEnchantManager().getPriceBySign(sign));
    }

    @Override
    public boolean hasEnough(Player player, Sign sign) {
        return  getCost(player, sign) != -1 && getPlugin().getEconomy().has(player.getName(), getCost(player, sign));
    }
}

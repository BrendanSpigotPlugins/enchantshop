package me.brendanleet.enchantshop.gui;

import lombok.Getter;
import me.brendanleet.enchantshop.EnchantShop;
import me.brendanleet.enchantshop.config.type.MessagesConfig;
import me.brendanleet.enchantshop.objects.PluginComponent;
import me.brendanleet.enchantshop.transaction.Transaction;
import me.brendanleet.enchantshop.util.ItemBuilder;
import me.brendanleet.enchantshop.util.StringUtility;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.block.Sign;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.entity.ItemMergeEvent;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.InventoryHolder;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.*;

public class EnchantsGUI extends PluginComponent implements InventoryHolder {

    public EnchantsGUI(EnchantShop plugin) {
        super(plugin, true);
    }

    @Getter
    public Inventory inventory;

    private Map<UUID, Inventory> inventories = new HashMap<>();

    public void init(Player player) {
        inventories.put(player.getUniqueId(), Bukkit.createInventory(getPlugin().getEnchantsGUI(), 54, "Registered Enchants"));
        getPlugin().getEnchantManager().getEnchantments().keySet().forEach(enchantment -> inventories.get(player.getUniqueId()).addItem(getIcon(enchantment)));
        player.openInventory(inventories.get(player.getUniqueId()));
    }

    @EventHandler
    public void onInventoryClick(InventoryClickEvent event) {
        Player player = (Player) event.getWhoClicked();
        if (inventories.containsKey(player.getUniqueId()) && inventories.get(player.getUniqueId()).equals(event.getInventory())) {
            if (event.getCurrentItem() == null) return;
            if (event.getCurrentItem().getItemMeta() == null) return;
            if (event.getInventory() == null) return;

            event.setCancelled(true);
        }
    }

    @EventHandler
    public void onInventoryClose(InventoryCloseEvent event) {
        if (inventories.containsKey(event.getPlayer().getUniqueId()))
            inventories.remove(event.getPlayer().getUniqueId());
    }

    private ItemStack getIcon(Enchantment enchantment) {
        List<String> aliases = getPlugin().getEnchantManager().getEnchantments().get(enchantment);
        ItemStack output = new ItemBuilder(Material.ENCHANTED_BOOK)
                .setName("&6" + enchantment.getName())
                .toItemStack();

        ItemMeta im = output.getItemMeta();
        List<String> lore = new ArrayList<>();
        aliases.forEach(alias -> lore.add(StringUtility.color("&7 - &e" + alias)));
        im.setLore(lore);
        output.setItemMeta(im);
        return output;
    }
}

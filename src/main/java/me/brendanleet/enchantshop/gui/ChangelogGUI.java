package me.brendanleet.enchantshop.gui;

import lombok.Getter;
import me.brendanleet.enchantshop.EnchantShop;
import me.brendanleet.enchantshop.objects.PluginComponent;
import me.brendanleet.enchantshop.patchnotes.PatchNote;
import me.brendanleet.enchantshop.patchnotes.PatchNoteType;
import me.brendanleet.enchantshop.util.ItemBuilder;
import me.brendanleet.enchantshop.util.StringUtility;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.ComponentBuilder;
import net.md_5.bungee.api.chat.HoverEvent;
import net.md_5.bungee.api.chat.TextComponent;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.InventoryHolder;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.*;
import java.util.stream.Collectors;

public class ChangelogGUI extends PluginComponent implements InventoryHolder {

    public ChangelogGUI(EnchantShop plugin) {
        super(plugin, true);
    }

    @Getter
    public Inventory inventory;

    private Map<UUID, Inventory> inventories = new HashMap<>();
    private Map<UUID, String> lastPatchNotesView = new HashMap<>();

    public void init(Player player, PatchNoteType type) {
        lastPatchNotesView.put(player.getUniqueId(), type.name());
        inventories.put(player.getUniqueId(), Bukkit.createInventory(getPlugin().getChangelogGUI(), 54, "Changelog for " + getPlugin().getUpdater().getCurrentVersionString()));
        runAsync(() -> {
            List<PatchNote> getBugFixes = getPlugin().getPatchNoteManager().getPatchNotes().stream()
                    .filter(patch -> patch.getType() == type)
                    .collect(Collectors.toList());

            List<PatchNote> getNewAdditions = getPlugin().getPatchNoteManager().getPatchNotes().stream()
                    .filter(patch -> patch.getType() == type)
                    .collect(Collectors.toList());

            List<PatchNote> getChanges = getPlugin().getPatchNoteManager().getPatchNotes().stream()
                    .filter(patch -> patch.getType() == type)
                    .collect(Collectors.toList());

            if (type == PatchNoteType.BUG_FIXES) {
                if (getBugFixes.size() == 0) {
                    player.sendMessage(ChatColor.RED + "There are no patch notes available for bug fixes");
                    return;
                }

                getBugFixes.forEach(patch -> inventories.get(player.getUniqueId()).addItem(getPatchNoteIcon(patch)));
            } else if (type == PatchNoteType.CHANGE) {
                if (getChanges.size() == 0) {
                    player.sendMessage(ChatColor.RED + "There are no patch notes available for changes");
                    return;
                }

                getChanges.forEach(patch -> inventories.get(player.getUniqueId()).addItem(getPatchNoteIcon(patch)));
            } else if (type == PatchNoteType.NEW_ADDITIONS) {
                if (getNewAdditions.size() == 0) {
                    player.sendMessage(ChatColor.RED + "There are no patch notes available for new additions");
                    return;
                }

                getNewAdditions.forEach(patch -> inventories.get(player.getUniqueId()).addItem(getPatchNoteIcon(patch)));
            } else {
                if (getPlugin().getPatchNoteManager().getPatchNotes().size() == 0) {
                    player.sendMessage(ChatColor.RED + "There are no patch notes available");
                    return;
                }

                getPlugin().getPatchNoteManager().getPatchNotes().forEach(patch -> inventories.get(player.getUniqueId()).addItem(getPatchNoteIcon(patch)));
            }
        });
        player.openInventory(inventories.get(player.getUniqueId()));
    }

    @EventHandler
    public void onInventoryClick(InventoryClickEvent event) {
        Player player = (Player) event.getWhoClicked();
        if (inventories.containsKey(player.getUniqueId()) && inventories.get(player.getUniqueId()).equals(event.getInventory())) {
            if (event.getCurrentItem() == null) return;
            if (event.getCurrentItem().getItemMeta() == null) return;
            if (event.getInventory() == null) return;
            event.setCancelled(true);

            PatchNote patchNote = getPlugin().getPatchNoteManager().getPatchByTitle(ChatColor.stripColor(event.getCurrentItem().getItemMeta().getDisplayName()));
            if (patchNote == null)
                return;

            player.closeInventory();
            player.sendMessage(" ");
            player.sendMessage(ChatColor.DARK_GRAY.toString() + ChatColor.STRIKETHROUGH + "------------------------------------------");
            player.sendMessage(StringUtility.color("&6Patch - " + patchNote.getTitle() + " &7(" + patchNote.getType().toString() + "&7)"));
            player.sendMessage(patchNote.getDescription());
            TextComponent reopenGUIMessage = new TextComponent(net.md_5.bungee.api.ChatColor.AQUA + "Click here to reopen the patchnotes GUI");
            reopenGUIMessage.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/enchantshop patchnotes " + lastPatchNotesView.get(player.getUniqueId())));
            player.spigot().sendMessage(reopenGUIMessage);
            player.sendMessage(ChatColor.DARK_GRAY.toString() + ChatColor.STRIKETHROUGH + "------------------------------------------");
            player.sendMessage(" ");
            lastPatchNotesView.remove(player.getUniqueId());
        }
    }

    @EventHandler
    public void onInventoryClose(InventoryCloseEvent event) {
        if (inventories.containsKey(event.getPlayer().getUniqueId()))
            inventories.remove(event.getPlayer().getUniqueId());
    }

    private ItemStack getPatchNoteIcon(PatchNote note) {
        return new ItemBuilder(Material.PAPER)
                .setName("&6" + note.getTitle())
                .addLoreLine("&ePatch type: &b" + note.getType().toString())
                .addLoreLine("&eClick here for more info")
                .toItemStack();
    }
}

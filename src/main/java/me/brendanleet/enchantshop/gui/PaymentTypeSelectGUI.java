package me.brendanleet.enchantshop.gui;

import lombok.Getter;
import me.brendanleet.enchantshop.EnchantShop;
import me.brendanleet.enchantshop.config.type.MessagesConfig;
import me.brendanleet.enchantshop.objects.PluginComponent;
import me.brendanleet.enchantshop.transaction.Transaction;
import me.brendanleet.enchantshop.util.ItemBuilder;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.block.Sign;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.InventoryHolder;
import org.bukkit.inventory.ItemStack;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class PaymentTypeSelectGUI extends PluginComponent implements InventoryHolder {

    public PaymentTypeSelectGUI(EnchantShop plugin) {
        super(plugin, true);
    }

    @Getter
    public Inventory inventory;

    private Map<UUID, Inventory> inventories = new HashMap<>();
    private Map<UUID, Sign> signEdit = new HashMap<>();

    public void init(Player player, Sign sign, String[] signLines) {
        inventories.put(player.getUniqueId(), Bukkit.createInventory(getPlugin().getPaymentTypeSelectGUI(), 27, "Select a payment method"));
        signEdit.put(player.getUniqueId(), sign);
        for (int i = 0; i < 4; i++)
            signEdit.get(player.getUniqueId()).setLine(i, signLines[i]);

        inventories.get(player.getUniqueId()).setItem(11, money());
        inventories.get(player.getUniqueId()).setItem(13, infoBook());
        inventories.get(player.getUniqueId()).setItem(15, xp());
        player.openInventory(inventories.get(player.getUniqueId()));
    }

    @EventHandler
    public void onInventoryClick(InventoryClickEvent event) {
        Player player = (Player) event.getWhoClicked();
        if (inventories.containsKey(player.getUniqueId()) && inventories.get(player.getUniqueId()).equals(event.getInventory())) {
            if (event.getCurrentItem() == null) return;
            if (event.getCurrentItem().getItemMeta() == null) return;
            if (event.getInventory() == null) return;

            event.setCancelled(true);

            if (event.getCurrentItem().isSimilar(xp())) {
                updateSign(player, getPlugin().getXpLevelTransaction());
                return;
            }

            if (event.getCurrentItem().isSimilar(money())) {
               updateSign(player, getPlugin().getMoneyTransaction());
            }

        }
    }

    private void updateSign(Player player, Transaction type) {
        switch (type.getTransactionType()) {
            case MONEY:
                signEdit.get(player.getUniqueId()).setLine(3, "$" + signEdit.get(player.getUniqueId()).getLine(3));
                player.sendMessage(MessagesConfig.getMessages().get("payment-type-set").replace("{type}", "Money"));
                signEdit.get(player.getUniqueId()).update();
                player.closeInventory();
                break;
            default:
                signEdit.get(player.getUniqueId()).setLine(3, signEdit.get(player.getUniqueId()).getLine(3) + " Levels");
                player.sendMessage(MessagesConfig.getMessages().get("payment-type-set").replace("{type}", "XP"));
                signEdit.get(player.getUniqueId()).update();
                player.closeInventory();
                break;
        }
    }

    @EventHandler
    public void onInventoryClose(InventoryCloseEvent event) {
        if (inventories.containsKey(event.getPlayer().getUniqueId())) {
            inventories.remove(event.getPlayer().getUniqueId());
            signEdit.remove(event.getPlayer().getUniqueId());
        }
    }

    private ItemStack money() {
        return new ItemBuilder(Material.PAPER)
                .setName("&6Money payment type")
                .addLoreLine("&eLeft-click to select")
                .toItemStack();
    }

    private ItemStack xp() {
        return new ItemBuilder(Material.ENCHANTED_BOOK)
                .setName("&6XP Level payment type")
                .addLoreLine("&eLeft-click to select")
                .toItemStack();
    }

    private ItemStack infoBook() {
        return new ItemBuilder(Material.BOOK)
                .toItemStack();
    }
}

package me.brendanleet.enchantshop.patchnotes;

public enum PatchNoteType {

    BUG_FIXES,
    NEW_ADDITIONS,
    CHANGE,
    ALL
}

package me.brendanleet.enchantshop.patchnotes;

import lombok.Getter;

public class PatchNote {

    public PatchNote(String title, String description, PatchNoteType type) {
        this.title = title;
        this.description = description;
        this.type = type;
    }

    @Getter
    private String title;

    @Getter
    private String description;

    @Getter
    private PatchNoteType type;
}

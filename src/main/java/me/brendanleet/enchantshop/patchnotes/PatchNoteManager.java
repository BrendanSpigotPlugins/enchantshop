package me.brendanleet.enchantshop.patchnotes;

import lombok.Getter;
import me.brendanleet.enchantshop.EnchantShop;
import me.brendanleet.enchantshop.objects.PluginComponent;

import java.util.ArrayList;

public class PatchNoteManager extends PluginComponent {

    public PatchNoteManager(EnchantShop plugin) {
        super(plugin);
        getPatchNotes().add(new PatchNote("Fixed a NPE", "There was an off chance that an error may occur when attempting to purchase an enchantment.",
                PatchNoteType.BUG_FIXES));

        getPatchNotes().add(new PatchNote("Added bStats", "bStats allows me to view basic information about the server in order to improve the plugin. Find out more at: " +
                "https://bstats.org/",
                PatchNoteType.NEW_ADDITIONS));

        getPatchNotes().add(new PatchNote("Permission to purchase enchant", "Added an optional permission required to purchase an enchantment", PatchNoteType.NEW_ADDITIONS));

        getPatchNotes().add(new PatchNote("/enchantshop patchnotes", "Server owners can now view what changes were made to the plugin in-game with a" +
                "GUI.",
                PatchNoteType.NEW_ADDITIONS));

        getPatchNotes().add(new PatchNote("/enchantshop checkupdate", "Server owners can now check if an update is required without needing " +
                "to view the resource on the website",
                PatchNoteType.NEW_ADDITIONS));

        getPatchNotes().add(new PatchNote("Configurable use-vault", "Some owners may have vault on their server and not want to use with it EnchantShop. " +
                "This option in the config allows them to have vault on the server without being prompted to choose a payment method if they never want to use " +
                "money as a payment type", PatchNoteType.CHANGE));

        getPatchNotes().add(new PatchNote("Shop creation adjustment", "The previous version allowed you to create a shop without specifying the enchantment or price, " +
                "which results in the plugin returning the shop has been created but when players interacted with it then it would throw errors such as no enchantment found, etc. " +
                "This version refuses to create a shop without inputting the values it needs.", PatchNoteType.CHANGE));
    }

    @Getter
    private ArrayList<PatchNote> patchNotes = new ArrayList<>();

    public PatchNote getPatchByTitle(String title) {
        return getPatchNotes().stream()
                .filter(c -> c.getTitle().equalsIgnoreCase(title))
                .findFirst()
                .orElse(null);
    }

    public PatchNote getPatchByDescription(String description) {
        return getPatchNotes().stream()
                .filter(c -> c.getDescription().equalsIgnoreCase(description))
                .findFirst()
                .orElse(null);
    }
}

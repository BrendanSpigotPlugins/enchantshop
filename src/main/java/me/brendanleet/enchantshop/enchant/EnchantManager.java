package me.brendanleet.enchantshop.enchant;

import lombok.Getter;
import lombok.Setter;
import me.brendanleet.enchantshop.EnchantShop;
import me.brendanleet.enchantshop.objects.PluginComponent;
import me.brendanleet.enchantshop.util.DataTypeUtility;
import me.brendanleet.enchantshop.util.StringUtility;
import org.bukkit.block.Sign;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;

import java.util.*;

public class EnchantManager extends PluginComponent {

    public EnchantManager(EnchantShop plugin) {
        super(plugin);
    }

    @Getter
    private Map<Enchantment, List<String>> enchantments = new HashMap<>();
    @Getter
    private Map<Enchantment, String> enchantmentPermissions = new HashMap<>();

    public boolean isEnchantSign(Sign sign) {
        return sign.getLine(0).equalsIgnoreCase(getPlugin().getDefaultConfig().getSignTitle());
    }

    public int getEnchantLevel(Sign sign) {
        if (isEnchantSign(sign) && isValidEnchant(sign) && DataTypeUtility.isInteger(sign.getLine(2)))
            return Integer.parseInt(sign.getLine(2));
        return 0;
    }

    public int getPriceBySign(Sign sign) {
        if (sign.getLine(3).contains("$")) {
            return Integer.parseInt(sign.getLine(3).replace("$", ""));
        } else {
            String[] line = sign.getLine(3).split(" ");
            if (!DataTypeUtility.isInteger(line[0]))
                return -1;
            return Integer.parseInt(line[0]);
        }
    }

    public int getPriceByString(String string) {
        if (string.contains("$")) {
            return Integer.parseInt(string.replace("$", ""));
        } else {
            String[] line = string.split(" ");
            if (!DataTypeUtility.isInteger(line[0]))
                return -1;
            return Integer.parseInt(line[0]);
        }
    }

    public boolean isValidEnchant(Sign sign) {
        return getEnchant(sign) != null;
    }

    public Enchantment getEnchant(Sign sign) {
        return getEnchant(sign.getLine(1));
    }

    public boolean shouldPayWithCash(Sign sign) {
        return sign.getLine(3).contains("$");
    }

    public Enchantment getEnchant(String alias) {
        if (Enchantment.getByName(alias) != null)
            return Enchantment.getByName(alias);

        for (Enchantment enchantment : getEnchantments().keySet())
            for (String string : getEnchantments().get(enchantment))
                if (string.equalsIgnoreCase(alias))
                    return enchantment;
        return null;
    }

    public boolean hasPermission(Player player, Enchantment enchantment) {
        if (!getPlugin().getEnchantsConfig().isUsingEnchantPermissions())
            return true;

        return player.hasPermission(getEnchantmentPermissions().get(enchantment));
    }
}
